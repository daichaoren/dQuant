/*
Navicat MySQL Data Transfer

Source Server         : 10.168.76.153
Source Server Version : 50613
Source Host           : 10.168.76.153:3306
Source Database       : upTrade

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2017-03-15 15:01:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bktest_result_analyze_dtl
-- ----------------------------
DROP TABLE IF EXISTS `bktest_result_analyze_dtl`;
CREATE TABLE `bktest_result_analyze_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradeId` varchar(64) NOT NULL,
  `date` datetime DEFAULT NULL,
  `totalReturn` double(18,4) DEFAULT NULL,
  `benchmark` double(18,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bktest_result_analyze_dtl_info_index1` (`tradeId`,`date`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6766 DEFAULT CHARSET=utf8 COMMENT='�ز���������ϸ��Ϣ��';

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_simutrade_bktest_sum_info
-- ----------------------------
DROP TABLE IF EXISTS `up_simutrade_bktest_sum_info`;
CREATE TABLE `up_simutrade_bktest_sum_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tradeId` varchar(64) DEFAULT NULL,
  `runTime` double(11,2) DEFAULT NULL,
  `initCash` double(18,2) DEFAULT NULL,
  `beginTime` date DEFAULT NULL,
  `endTime` date DEFAULT NULL,
  `totalReturn` double(18,4) DEFAULT NULL,
  `benchmark` double(18,4) DEFAULT NULL,
  `alpha` double(18,4) DEFAULT NULL,
  `beta` double(18,4) DEFAULT NULL,
  `sharpe` double(18,4) DEFAULT NULL,
  `infoRatio` double(18,4) DEFAULT NULL,
  `sortino` double(18,4) DEFAULT NULL,
  `benchmarkAnnualReturn` double(18,4) DEFAULT NULL,
  `myAnnualReturn` double(18,4) DEFAULT NULL,
  `volatility` double(18,4) DEFAULT NULL,
  `maxDrewdown` double(18,4) DEFAULT NULL,
  `trackingError` double(18,4) DEFAULT NULL,
  `downsideRisk` double(18,4) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_bktest_sum_info_index1` (`tradeId`)
) ENGINE=InnoDB AUTO_INCREMENT=1302 DEFAULT CHARSET=utf8 COMMENT='Up����ƽ̨��ʷ�ز����������Ϣ��';

-- ----------------------------
-- Table structure for up_simutrade_latest_positioninfo
-- ----------------------------
DROP TABLE IF EXISTS `up_simutrade_latest_positioninfo`;
CREATE TABLE `up_simutrade_latest_positioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradeId` varchar(64) NOT NULL,
  `stockCode` varchar(20) DEFAULT NULL,
  `stockName` varchar(32) DEFAULT NULL,
  `close` double(18,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `marketvalue` double(18,2) DEFAULT NULL,
  `return` double(18,4) DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_tradeinfo_index1` (`tradeId`,`stockCode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5113 DEFAULT CHARSET=utf8 COMMENT='UP����ƽ̨������Ϣ�����¼����ÿ�ճֲ��Լ��������\r\n';

-- ----------------------------
-- Table structure for up_simutrade_loginfo
-- ----------------------------
DROP TABLE IF EXISTS `up_simutrade_loginfo`;
CREATE TABLE `up_simutrade_loginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradeid` varchar(64) DEFAULT NULL,
  `log` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_simutrade_loginfo_ix1` (`tradeid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_simutrade_positioninfo
-- ----------------------------
DROP TABLE IF EXISTS `up_simutrade_positioninfo`;
CREATE TABLE `up_simutrade_positioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradeId` varchar(64) NOT NULL,
  `date` date DEFAULT NULL,
  `stockCode` varchar(20) DEFAULT NULL,
  `stockName` varchar(32) DEFAULT NULL,
  `close` double(18,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `marketvalue` double(18,2) DEFAULT NULL,
  `return` double(18,4) DEFAULT NULL,
  `portfolio_value` double(18,2) DEFAULT NULL,
  `cash` double(18,2) DEFAULT NULL,
  `totalcost` double(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_tradeinfo_index1` (`tradeId`,`date`,`stockCode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17662 DEFAULT CHARSET=utf8 COMMENT='UP����ƽ̨������Ϣ�����¼����ÿ�ճֲ��Լ��������\r\n';

-- ----------------------------
-- Table structure for up_simutrade_tradeinfo
-- ----------------------------
DROP TABLE IF EXISTS `up_simutrade_tradeinfo`;
CREATE TABLE `up_simutrade_tradeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradeId` varchar(64) NOT NULL,
  `orderId` varchar(32) NOT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` time DEFAULT NULL,
  `stockCode` varchar(20) DEFAULT NULL,
  `stockName` varchar(32) DEFAULT NULL,
  `orderState` int(11) DEFAULT NULL,
  `orderCount` int(11) DEFAULT NULL,
  `price` double(18,2) DEFAULT NULL,
  `totalCost` double(18,2) DEFAULT NULL,
  `commission` double(18,2) DEFAULT NULL,
  `tax` double(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_tradeinfo_index1` (`tradeId`,`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=5667 DEFAULT CHARSET=utf8 COMMENT='UP����ƽ̨������Ϣ�����¼����ÿ�ճֲ��Լ��������\r\n';
