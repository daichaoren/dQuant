/*
Navicat MySQL Data Transfer

Source Server         : 10.168.76.153
Source Server Version : 50613
Source Host           : 10.168.76.153:3306
Source Database       : upQuant

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2017-03-15 15:01:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bktest_result_analyze_dtl_info
-- ----------------------------
DROP TABLE IF EXISTS `bktest_result_analyze_dtl_info`;
CREATE TABLE `bktest_result_analyze_dtl_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strategyId` varchar(32) NOT NULL,
  `versionID` int(11) NOT NULL,
  `datetime` date DEFAULT NULL,
  `totalReturn` double(18,4) DEFAULT NULL,
  `benchmark` double(18,4) DEFAULT NULL,
  `alpha` double(18,4) DEFAULT NULL,
  `beta` double(18,4) DEFAULT NULL,
  `sharpe` double(18,4) DEFAULT NULL,
  `sortino` double(18,4) DEFAULT NULL,
  `infoRatio` double(18,4) DEFAULT NULL,
  `volatility` double(18,4) DEFAULT NULL,
  `maxDrewdown` double(18,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bktest_result_analyze_dtl_info_index1` (`strategyId`,`versionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='�ز���������ϸ��Ϣ��';

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_quant_bktest_sum_info
-- ----------------------------
DROP TABLE IF EXISTS `up_quant_bktest_sum_info`;
CREATE TABLE `up_quant_bktest_sum_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `strategyID` varchar(32) DEFAULT NULL,
  `versionID` int(11) DEFAULT NULL,
  `runTime` int(11) DEFAULT NULL,
  `initCash` double(18,4) DEFAULT NULL,
  `beginTime` date DEFAULT NULL,
  `endTime` date DEFAULT NULL,
  `totalReturn` double(18,4) DEFAULT NULL,
  `benchmark` double(18,4) DEFAULT NULL,
  `alpha` double(18,4) DEFAULT NULL,
  `beta` double(18,4) DEFAULT NULL,
  `sharpe` double(18,4) DEFAULT NULL,
  `infoRatio` double(18,4) DEFAULT NULL,
  `sortino` double(18,4) DEFAULT NULL,
  `benchmarkAnnualReturn` double(18,4) DEFAULT NULL,
  `myAnnualReturn` double(18,4) DEFAULT NULL,
  `volatility` double(18,4) DEFAULT NULL,
  `maxDrewdown` double(18,4) DEFAULT NULL,
  `trackingError` double(18,4) DEFAULT NULL,
  `downsideRisk` double(18,4) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_bktest_sum_info_index1` (`strategyID`,`versionID`)
) ENGINE=InnoDB AUTO_INCREMENT=1777 DEFAULT CHARSET=utf8 COMMENT='Up����ƽ̨��ʷ�ز����������Ϣ��';

-- ----------------------------
-- Table structure for up_quant_simutrade_strategylist
-- ----------------------------
DROP TABLE IF EXISTS `up_quant_simutrade_strategylist`;
CREATE TABLE `up_quant_simutrade_strategylist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(32) DEFAULT NULL,
  `tradeID` varchar(64) DEFAULT NULL,
  `tradeName` varchar(32) DEFAULT NULL,
  `strategyID` varchar(32) DEFAULT NULL,
  `versionID` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `initCash` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `up_quant_simutrade_strategy_ix1` (`tradeID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_quant_strategy_contex
-- ----------------------------
DROP TABLE IF EXISTS `up_quant_strategy_contex`;
CREATE TABLE `up_quant_strategy_contex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strategyId` varchar(32) NOT NULL,
  `versionId` int(11) NOT NULL,
  `begindate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `initcash` double DEFAULT NULL,
  `context` text,
  `createtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_strategy_contex_index1` (`strategyId`,`versionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1864 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_quant_tradeinfo
-- ----------------------------
DROP TABLE IF EXISTS `up_quant_tradeinfo`;
CREATE TABLE `up_quant_tradeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strategyId` varchar(32) NOT NULL,
  `versionId` int(11) NOT NULL,
  `orderId` varchar(32) NOT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` time DEFAULT NULL,
  `stockCode` varchar(20) DEFAULT NULL,
  `stockName` varchar(32) DEFAULT NULL,
  `orderState` int(11) DEFAULT NULL,
  `orderCount` int(11) DEFAULT NULL,
  `price` double(18,4) DEFAULT NULL,
  `totalCost` double(18,4) DEFAULT NULL,
  `commission` double(18,4) DEFAULT NULL,
  `tax` double(18,4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_tradeinfo_index1` (`strategyId`,`versionId`,`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=262823 DEFAULT CHARSET=utf8 COMMENT='UP����ƽ̨������Ϣ�����¼����ÿ�ճֲ��Լ��������\r\n';

-- ----------------------------
-- Table structure for up_quant_user_strategy_list_info
-- ----------------------------
DROP TABLE IF EXISTS `up_quant_user_strategy_list_info`;
CREATE TABLE `up_quant_user_strategy_list_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL,
  `strategyId` varchar(32) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `up_quant_user_strategy_list_info_index1` (`userid`,`strategyId`)
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8 COMMENT='Up��Ʒ����ƽ̨�û�������Ϣ��';
