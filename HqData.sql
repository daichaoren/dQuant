/*
Navicat MySQL Data Transfer

Source Server         : 10.168.76.153
Source Server Version : 50613
Source Host           : 10.168.76.153:3306
Source Database       : HqData

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2017-03-15 15:01:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for com_share_stru
-- ----------------------------
DROP TABLE IF EXISTS `com_share_stru`;
CREATE TABLE `com_share_stru` (
  `index` bigint(20) DEFAULT NULL,
  `STK_CODE` text,
  `SEC_MAR_PAR` bigint(20) DEFAULT NULL,
  `ISVALID` bigint(20) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `COM_UNI_CODE` bigint(20) DEFAULT NULL,
  `CHAN_DATE` datetime DEFAULT NULL,
  `DECL_DATE` datetime DEFAULT NULL,
  `CHAN_REAS_PAR` bigint(20) DEFAULT NULL,
  `CHAN_REAS_REMA` text,
  `TOT_SHARE` bigint(20) DEFAULT NULL,
  `FLOAT_SHARE` double DEFAULT NULL,
  `A_FLOAT_SHARE` double DEFAULT NULL,
  `LIST_FLOAT_ASHR` double DEFAULT NULL,
  `LIST_FLOAT_MAN` double DEFAULT NULL,
  `B_SHARE` double DEFAULT NULL,
  `H_SHARE` double DEFAULT NULL,
  `S_SHARE` double DEFAULT NULL,
  `N_SHARE` text,
  `OTH_ABRO_SHARE` double DEFAULT NULL,
  `OTH_FLOAT_SHARE` double DEFAULT NULL,
  `TOT_REST` double DEFAULT NULL,
  `TOT_STATE_LEG_REST` double DEFAULT NULL,
  `STATE_REST` double DEFAULT NULL,
  `STATE_LEG_REST` double DEFAULT NULL,
  `DOM_LEG_REST` double DEFAULT NULL,
  `DOM_NATU_REST` double DEFAULT NULL,
  `FORE_REST` double DEFAULT NULL,
  `FORE_LEG_RES` double DEFAULT NULL,
  `FORE_NATU_RES` double DEFAULT NULL,
  `OTH_SPON_RES` double DEFAULT NULL,
  `RAIS_LEG_RES` double DEFAULT NULL,
  `INNER_STAFF_RES` double DEFAULT NULL,
  `MAN_RES` double DEFAULT NULL,
  `PREF_RES` double DEFAULT NULL,
  `TRANS_RES` double DEFAULT NULL,
  `OTH_RES` double DEFAULT NULL,
  `STRA_PLACE_RES` double DEFAULT NULL,
  `FUND_PLACE_RES` double DEFAULT NULL,
  `LEG_PLACE_RES` double DEFAULT NULL,
  `TOT_NFLOAT` double DEFAULT NULL,
  `TOT_STATE_LEG_NFLOAT` double DEFAULT NULL,
  `STATE_NFLOAT` double DEFAULT NULL,
  `STATE_LEG_NFLOAT` double DEFAULT NULL,
  `DOM_LEG_NFLOAT` double DEFAULT NULL,
  `DOM_NATU_NFLOAT` double DEFAULT NULL,
  `FORE_NFLOAT` double DEFAULT NULL,
  `FORE_LEG_NFLOAT` double DEFAULT NULL,
  `FORE_NATU_NFLOAT` double DEFAULT NULL,
  `OTH_SPON_NFLOAT` double DEFAULT NULL,
  `RAIS_LEG_NFLOAT` double DEFAULT NULL,
  `INNER_STAFF_NFLOAT` double DEFAULT NULL,
  `MAN_NFLOAT` double DEFAULT NULL,
  `PREF_NFLOAT` double DEFAULT NULL,
  `TRANS_NFLOAT` double DEFAULT NULL,
  `OTH_NFLOAT` double DEFAULT NULL,
  `MAIN_HOLDER_SHR_PROP` double DEFAULT NULL,
  `CIR_HOLDER_SHR_PROP` double DEFAULT NULL,
  `A_SHARE` bigint(20) DEFAULT NULL,
  KEY `ix_com_share_stru_index` (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for fin_idx_ana
-- ----------------------------
DROP TABLE IF EXISTS `fin_idx_ana`;
CREATE TABLE `fin_idx_ana` (
  `index` bigint(20) DEFAULT NULL,
  `STK_CODE` text,
  `SEC_MAR_PAR` bigint(20) DEFAULT NULL,
  `ISVALID` bigint(20) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `UPDATETIME` datetime DEFAULT NULL,
  `COM_UNI_CODE` bigint(20) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `BEPS` double DEFAULT NULL,
  `DEPS` double DEFAULT NULL,
  `EPSED` double DEFAULT NULL,
  `EPSNED` double DEFAULT NULL,
  `BEPS_DED` double DEFAULT NULL,
  `DEPS_DED` double DEFAULT NULL,
  `EPSED_DED` double DEFAULT NULL,
  `BPS` double DEFAULT NULL,
  `BPSNED` double DEFAULT NULL,
  `PS_OCF` double DEFAULT NULL,
  `PS_TOR` double DEFAULT NULL,
  `PS_OR` double DEFAULT NULL,
  `PS_CR` double DEFAULT NULL,
  `PS_SR` double DEFAULT NULL,
  `PS_UP` double DEFAULT NULL,
  `PS_RE` double DEFAULT NULL,
  `PS_CN` double DEFAULT NULL,
  `PS_EBIT` double DEFAULT NULL,
  `PS_DIV` double DEFAULT NULL,
  `ROEA` double DEFAULT NULL,
  `ROEW` double DEFAULT NULL,
  `ROED` double DEFAULT NULL,
  `ROEA_DED` double DEFAULT NULL,
  `ROEW_DED` double DEFAULT NULL,
  `ROED_DED` double DEFAULT NULL,
  `ROE_AIC` double DEFAULT NULL,
  `ROA` double DEFAULT NULL,
  `ROA_NP` double DEFAULT NULL,
  `ROIC` double DEFAULT NULL,
  `ROE_YEAR` double DEFAULT NULL,
  `ROA_YEAR` double DEFAULT NULL,
  `ROA_NYEAR` double DEFAULT NULL,
  `SAL_NPR` double DEFAULT NULL,
  `SAL_GIR` double DEFAULT NULL,
  `SAL_COST` double DEFAULT NULL,
  `SAL_PFR` double DEFAULT NULL,
  `TR_NP` double DEFAULT NULL,
  `TR_OP` double DEFAULT NULL,
  `TR_EBIT` double DEFAULT NULL,
  `TR_TC` double DEFAULT NULL,
  `TR_OF` double DEFAULT NULL,
  `TR_MF` double DEFAULT NULL,
  `TR_FF` double DEFAULT NULL,
  `TR_AL` double DEFAULT NULL,
  `TP_ONI` double DEFAULT NULL,
  `TP_VCI` double DEFAULT NULL,
  `TP_NON` double DEFAULT NULL,
  `TP_TAX` double DEFAULT NULL,
  `TP_DNP` double DEFAULT NULL,
  `OR_SAL` double DEFAULT NULL,
  `OR_OCF` double DEFAULT NULL,
  `OR_ONI` double DEFAULT NULL,
  `ASS_DEBT` double DEFAULT NULL,
  `EM` double DEFAULT NULL,
  `TA_CA` double DEFAULT NULL,
  `TA_NCA` double DEFAULT NULL,
  `TA_TA` double DEFAULT NULL,
  `TC_PCE` double DEFAULT NULL,
  `TC_IBD` double DEFAULT NULL,
  `TL_CL` double DEFAULT NULL,
  `TL_NCL` double DEFAULT NULL,
  `CR` double DEFAULT NULL,
  `QR` double DEFAULT NULL,
  `KQR` double DEFAULT NULL,
  `ER` double DEFAULT NULL,
  `TL_PCE` double DEFAULT NULL,
  `IBD_PCE` double DEFAULT NULL,
  `TL_TA` double DEFAULT NULL,
  `IBD_TA` double DEFAULT NULL,
  `ND_TA` double DEFAULT NULL,
  `TL_OCF` double DEFAULT NULL,
  `IBD_OCF` double DEFAULT NULL,
  `CL_OCF` double DEFAULT NULL,
  `ND_OCF` double DEFAULT NULL,
  `IF_EBIT` double DEFAULT NULL,
  `WK_LD` double DEFAULT NULL,
  `OPE_CYC` double DEFAULT NULL,
  `INV_DAYS` double DEFAULT NULL,
  `ARC_DAYS` double DEFAULT NULL,
  `INV_RATE` double DEFAULT NULL,
  `ARC_RATE` double DEFAULT NULL,
  `CA_RATE` double DEFAULT NULL,
  `FA_RATE` double DEFAULT NULL,
  `TA_RATE` double DEFAULT NULL,
  `AP_RATE` double DEFAULT NULL,
  `AP_DAYS` double DEFAULT NULL,
  `BEPS_YOY` double DEFAULT NULL,
  `DEPS_YOY` double DEFAULT NULL,
  `PS_OCF_YOY` double DEFAULT NULL,
  `TR_YOY` double DEFAULT NULL,
  `OR_YOY` double DEFAULT NULL,
  `OP_YOY` double DEFAULT NULL,
  `TP_YOY` double DEFAULT NULL,
  `PCNP_YOY` double DEFAULT NULL,
  `PCNDP_YOY` double DEFAULT NULL,
  `OCF_YOY` double DEFAULT NULL,
  `ROE_YOY` double DEFAULT NULL,
  `BPS_YTD` double DEFAULT NULL,
  `TA_YTD` double DEFAULT NULL,
  `PCE_YTD` double DEFAULT NULL,
  `INC_A` double DEFAULT NULL,
  `INC_B` double DEFAULT NULL,
  `INC_C` double DEFAULT NULL,
  `INC_D` double DEFAULT NULL,
  `INC_E` double DEFAULT NULL,
  `INC_F` double DEFAULT NULL,
  `INC_H` double DEFAULT NULL,
  `INC_I` double DEFAULT NULL,
  `INC_J` double DEFAULT NULL,
  `BAL_A` double DEFAULT NULL,
  `BAL_B` double DEFAULT NULL,
  `BAL_C` double DEFAULT NULL,
  `BAL_D` double DEFAULT NULL,
  `BAL_E` double DEFAULT NULL,
  `BAL_F` double DEFAULT NULL,
  `BAL_G` double DEFAULT NULL,
  `BAL_H` double DEFAULT NULL,
  `BAL_I` double DEFAULT NULL,
  `BAL_M` double DEFAULT NULL,
  `BAL_N` double DEFAULT NULL,
  `BAL_O` double DEFAULT NULL,
  `BAL_P` double DEFAULT NULL,
  KEY `ix_fin_idx_ana_index` (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `stock_baseinfo`;
CREATE TABLE `stock_baseinfo` (
  `code` varchar(32) DEFAULT NULL,
  `market` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `industry` varchar(32) DEFAULT NULL,
  `area` varchar(32) DEFAULT NULL,
  `pe` varchar(32) DEFAULT NULL,
  `outstanding` double DEFAULT NULL,
  `totals` double DEFAULT NULL,
  `totalAssets` double DEFAULT NULL,
  `liquidAssets` double DEFAULT NULL,
  `fixedAssets` double DEFAULT NULL,
  `reserved` double DEFAULT NULL,
  `reservedPerShare` double DEFAULT NULL,
  `esp` double DEFAULT NULL,
  `bvps` double DEFAULT NULL,
  `pb` double DEFAULT NULL,
  `timeToMarket` bigint(20) DEFAULT NULL,
  KEY `ix_stock_baseinfo_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_divident_factor
-- ----------------------------
DROP TABLE IF EXISTS `stock_divident_factor`;
CREATE TABLE `stock_divident_factor` (
  `code` varchar(32) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `dividendCash` double DEFAULT NULL,
  `dividendNum` double DEFAULT NULL,
  `divitype` bigint(20) DEFAULT NULL,
  `rationedNum` double DEFAULT NULL,
  `rationedPrice` double DEFAULT NULL,
  `seoNum` double DEFAULT NULL,
  `seoPrice` double DEFAULT NULL,
  `trunNum` double DEFAULT NULL,
  KEY `ix_stock_divident_factor_code` (`code`),
  KEY `ix_stock_divident_factor_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_his_daykline
-- ----------------------------
DROP TABLE IF EXISTS `stock_his_daykline`;
CREATE TABLE `stock_his_daykline` (
  `date` date NOT NULL,
  `code` varchar(32) NOT NULL,
  `open` double DEFAULT NULL,
  `high` double DEFAULT NULL,
  `close` double DEFAULT NULL,
  `low` double DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`code`,`date`),
  KEY `ix_stock_his_daykline_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_hiskline
-- ----------------------------
DROP TABLE IF EXISTS `stock_hiskline`;
CREATE TABLE `stock_hiskline` (
  `date` datetime DEFAULT NULL,
  `open` double DEFAULT NULL,
  `high` double DEFAULT NULL,
  `close` double DEFAULT NULL,
  `low` double DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  KEY `ix_stock_hiskline_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_block_info
-- ----------------------------
DROP TABLE IF EXISTS `up_block_info`;
CREATE TABLE `up_block_info` (
  `blockcode` varchar(32) DEFAULT NULL,
  `stockcode` varchar(32) DEFAULT NULL,
  `blockname` varchar(32) DEFAULT NULL,
  `num` bigint(20) DEFAULT NULL,
  `stockname` varchar(32) DEFAULT NULL,
  KEY `ix_up_block_info_blockcode` (`blockcode`),
  KEY `ix_up_block_info_stockcode` (`stockcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for up_trade_calender
-- ----------------------------
DROP TABLE IF EXISTS `up_trade_calender`;
CREATE TABLE `up_trade_calender` (
  `date` date DEFAULT NULL,
  `market` bigint(20) DEFAULT NULL,
  `tradeflag` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
