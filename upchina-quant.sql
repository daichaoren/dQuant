/*
Navicat MySQL Data Transfer

Source Server         : 10.168.76.153
Source Server Version : 50613
Source Host           : 10.168.76.153:3306
Source Database       : upchina-quant

Target Server Type    : MYSQL
Target Server Version : 50613
File Encoding         : 65001

Date: 2017-03-15 15:07:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class_main
-- ----------------------------
DROP TABLE IF EXISTS `class_main`;
CREATE TABLE `class_main` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(64) DEFAULT NULL,
  `NODE` varchar(64) DEFAULT NULL,
  `NUM` int(11) DEFAULT NULL,
  `URL` varchar(128) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='CLASS_MAIN';

-- ----------------------------
-- Table structure for doc_main
-- ----------------------------
DROP TABLE IF EXISTS `doc_main`;
CREATE TABLE `doc_main` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='DOC_MAIN';

-- ----------------------------
-- Table structure for user_login
-- ----------------------------
DROP TABLE IF EXISTS `user_login`;
CREATE TABLE `user_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
