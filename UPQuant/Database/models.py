# coding=utf-8
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BktestResultAnalyzeDtlInfo(models.Model):
    strategyid = models.CharField(db_column='strategyId', max_length=32)  # Field name made lowercase.
    versionid = models.IntegerField(db_column='versionID')  # Field name made lowercase.
    datetime = models.DateField(blank=True, null=True)
    totalreturn = models.FloatField(db_column='totalReturn', blank=True, null=True)  # Field name made lowercase.
    benchmark = models.FloatField(blank=True, null=True)
    alpha = models.FloatField(blank=True, null=True)
    beta = models.FloatField(blank=True, null=True)
    sharpe = models.FloatField(blank=True, null=True)
    sortino = models.FloatField(blank=True, null=True)
    inforatio = models.FloatField(db_column='infoRatio', blank=True, null=True)  # Field name made lowercase.
    volatility = models.FloatField(blank=True, null=True)
    maxdrewdown = models.FloatField(db_column='maxDrewdown', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bktest_result_analyze_dtl_info'
        unique_together = (('strategyid', 'versionid'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class UpQuantBktestSumInfo(models.Model):
    strategyid = models.CharField(db_column='strategyID', max_length=32, blank=True,
                                  null=True)  # Field name made lowercase.
    versionid = models.IntegerField(db_column='versionID', blank=True, null=True)  # Field name made lowercase.
    runtime = models.IntegerField(db_column='runTime', blank=True, null=True)  # Field name made lowercase.
    initcash = models.FloatField(db_column='initCash', blank=True, null=True)  # Field name made lowercase.
    begintime = models.DateField(db_column='beginTime', blank=True, null=True)  # Field name made lowercase.
    endtime = models.DateField(db_column='endTime', blank=True, null=True)  # Field name made lowercase.
    totalreturn = models.FloatField(db_column='totalReturn', blank=True, null=True)  # Field name made lowercase.
    benchmark = models.FloatField(blank=True, null=True)
    alpha = models.FloatField(blank=True, null=True)
    beta = models.FloatField(blank=True, null=True)
    sharpe = models.FloatField(blank=True, null=True)
    inforatio = models.FloatField(db_column='infoRatio', blank=True, null=True)  # Field name made lowercase.
    sortino = models.FloatField(blank=True, null=True)
    benchmarkannualreturn = models.FloatField(db_column='benchmarkAnnualReturn', blank=True,
                                              null=True)  # Field name made lowercase.
    myannualreturn = models.FloatField(db_column='myAnnualReturn', blank=True, null=True)  # Field name made lowercase.
    volatility = models.FloatField(blank=True, null=True)
    maxdrewdown = models.FloatField(db_column='maxDrewdown', blank=True, null=True)  # Field name made lowercase.
    trackingerror = models.FloatField(db_column='trackingError', blank=True, null=True)  # Field name made lowercase.
    downsiderisk = models.FloatField(db_column='downsideRisk', blank=True, null=True)  # Field name made lowercase.

    # createtime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_quant_bktest_sum_info'
        unique_together = (('strategyid', 'versionid'),)


class UpQuantStrategyContex(models.Model):
    strategyid = models.CharField(db_column='strategyId', max_length=32)  # Field name made lowercase.
    versionid = models.IntegerField(db_column='versionId')  # Field name made lowercase.
    begindate = models.DateField(blank=True, null=True)
    enddate = models.DateField(blank=True, null=True)
    initcash = models.FloatField(blank=True, null=True)
    context = models.TextField(blank=True, null=True)
    createtime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_quant_strategy_contex'
        unique_together = (('strategyid', 'versionid'),)


class UpQuantTradeinfo(models.Model):
    strategyid = models.CharField(db_column='strategyId', max_length=32)  # Field name made lowercase.
    versionid = models.IntegerField(db_column='versionId')  # Field name made lowercase.
    orderid = models.CharField(db_column='orderId', max_length=32)  # Field name made lowercase.
    orderdate = models.DateField(db_column='orderDate', blank=True, null=True)  # Field name made lowercase.
    ordertime = models.TimeField(db_column='orderTime', blank=True, null=True)  # Field name made lowercase.
    stockcode = models.CharField(db_column='stockCode', max_length=20, blank=True,
                                 null=True)  # Field name made lowercase.
    stockname = models.CharField(db_column='stockName', max_length=32, blank=True,
                                 null=True)  # Field name made lowercase.
    orderstate = models.IntegerField(db_column='orderState', blank=True, null=True)  # Field name made lowercase.
    ordercount = models.IntegerField(db_column='orderCount', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(blank=True, null=True)
    totalcost = models.FloatField(db_column='totalCost', blank=True, null=True)  # Field name made lowercase.
    commission = models.FloatField(blank=True, null=True)
    tax = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_quant_tradeinfo'
        unique_together = (('strategyid', 'versionid', 'orderid'),)


class UpQuantUserStrategyListInfo(models.Model):
    userid = models.CharField(max_length=32)
    strategyid = models.CharField(db_column='strategyId', max_length=32)  # Field name made lowercase.
    name = models.CharField(max_length=128, blank=True, null=True)
    createtime = models.DateTimeField(blank=True, null=True)
    modifytime = models.DateTimeField(db_column='modifyTime', blank=True, null=True)  # Field name made lowercase.
    state = models.IntegerField(blank=True, null=True)
    num = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_quant_user_strategy_list_info'
        unique_together = (('userid', 'strategyid'),)


class UpQuantSimutradeStrategylist(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    userid = models.CharField(db_column='UserID', max_length=32, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.CharField(db_column='tradeID', unique=True, max_length=32, blank=True,
                               null=True)  # Field name made lowercase.
    tradename = models.CharField(db_column='tradeName', max_length=32, blank=True,
                                 null=True)  # Field name made lowercase.
    strategyid = models.CharField(db_column='strategyID', max_length=32, blank=True,
                                  null=True)  # Field name made lowercase.
    versionid = models.IntegerField(db_column='versionID', blank=True, null=True)  # Field name made lowercase.
    startdate = models.DateTimeField(blank=True, null=True)
    enddate = models.DateTimeField(blank=True, null=True)
    initcash = models.IntegerField(db_column='initCash', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_quant_simutrade_strategylist'


# 交易数据库的表
class BktestResultAnalyzeDtl(models.Model):
    tradeid = models.CharField(db_column='tradeId', max_length=64)  # Field name made lowercase.
    date = models.DateTimeField(blank=True, null=True)
    totalreturn = models.FloatField(db_column='totalReturn', blank=True, null=True)  # Field name made lowercase.
    benchmark = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bktest_result_analyze_dtl'
        unique_together = (('tradeid', 'date'),)


class UpSimutradeBktestSumInfo(models.Model):
    tradeid = models.CharField(db_column='tradeId', unique=True, max_length=64, blank=True, null=True)  # Field name made lowercase.
    runtime = models.FloatField(db_column='runTime', blank=True, null=True)  # Field name made lowercase.
    initcash = models.FloatField(db_column='initCash', blank=True, null=True)  # Field name made lowercase.
    begintime = models.DateField(db_column='beginTime', blank=True, null=True)  # Field name made lowercase.
    endtime = models.DateField(db_column='endTime', blank=True, null=True)  # Field name made lowercase.
    totalreturn = models.FloatField(db_column='totalReturn', blank=True, null=True)  # Field name made lowercase.
    benchmark = models.FloatField(blank=True, null=True)
    alpha = models.FloatField(blank=True, null=True)
    beta = models.FloatField(blank=True, null=True)
    sharpe = models.FloatField(blank=True, null=True)
    inforatio = models.FloatField(db_column='infoRatio', blank=True, null=True)  # Field name made lowercase.
    sortino = models.FloatField(blank=True, null=True)
    benchmarkannualreturn = models.FloatField(db_column='benchmarkAnnualReturn', blank=True, null=True)  # Field name made lowercase.
    myannualreturn = models.FloatField(db_column='myAnnualReturn', blank=True, null=True)  # Field name made lowercase.
    volatility = models.FloatField(blank=True, null=True)
    maxdrewdown = models.FloatField(db_column='maxDrewdown', blank=True, null=True)  # Field name made lowercase.
    trackingerror = models.FloatField(db_column='trackingError', blank=True, null=True)  # Field name made lowercase.
    downsiderisk = models.FloatField(db_column='downsideRisk', blank=True, null=True)  # Field name made lowercase.
    createtime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_simutrade_bktest_sum_info'


class UpSimutradeLatestPositioninfo(models.Model):
    tradeid = models.CharField(db_column='tradeId', max_length=64)  # Field name made lowercase.
    stockcode = models.CharField(db_column='stockCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stockname = models.CharField(db_column='stockName', max_length=32, blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    marketvalue = models.FloatField(blank=True, null=True)
    return_field = models.FloatField(db_column='return', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    updatetime = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'up_simutrade_latest_positioninfo'
        unique_together = (('tradeid', 'stockcode'),)


class UpSimutradeLoginfo(models.Model):
    tradeid = models.CharField(unique=True, max_length=64, blank=True, null=True)
    log = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_simutrade_loginfo'


class UpSimutradePositioninfo(models.Model):
    tradeid = models.CharField(db_column='tradeId', max_length=64)  # Field name made lowercase.
    date = models.DateField(blank=True, null=True)
    stockcode = models.CharField(db_column='stockCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stockname = models.CharField(db_column='stockName', max_length=32, blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    marketvalue = models.FloatField(blank=True, null=True)
    return_field = models.FloatField(db_column='return', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    portfolio_value = models.FloatField(blank=True, null=True)
    cash = models.FloatField(blank=True, null=True)
    totalcost = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_simutrade_positioninfo'
        unique_together = (('tradeid', 'date', 'stockcode'),)


class UpSimutradeTradeinfo(models.Model):
    tradeid = models.CharField(db_column='tradeId', max_length=64)  # Field name made lowercase.
    orderid = models.CharField(db_column='orderId', max_length=32)  # Field name made lowercase.
    orderdate = models.DateField(db_column='orderDate', blank=True, null=True)  # Field name made lowercase.
    ordertime = models.TimeField(db_column='orderTime', blank=True, null=True)  # Field name made lowercase.
    stockcode = models.CharField(db_column='stockCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stockname = models.CharField(db_column='stockName', max_length=32, blank=True, null=True)  # Field name made lowercase.
    orderstate = models.IntegerField(db_column='orderState', blank=True, null=True)  # Field name made lowercase.
    ordercount = models.IntegerField(db_column='orderCount', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(blank=True, null=True)
    totalcost = models.FloatField(db_column='totalCost', blank=True, null=True)  # Field name made lowercase.
    commission = models.FloatField(blank=True, null=True)
    tax = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'up_simutrade_tradeinfo'
        unique_together = (('tradeid', 'orderid'),)
