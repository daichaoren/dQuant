import django_filters
from Database.models import UpQuantUserStrategyListInfo, UpQuantBktestSumInfo


class BkTestInfoFilter(django_filters.FilterSet):
    strategy = django_filters.CharFilter(name="strategy", lookup_type="contains")
    strategy = django_filters.CharFilter(name="strategyid", lookup_type="contains")

    class Meta:
        model = UpQuantBktestSumInfo
        fields = ('strategy', 'strategyid', 'totalreturn', 'alpha', 'beta', 'sharpe', 'volatility', 'maxdrewdown')
