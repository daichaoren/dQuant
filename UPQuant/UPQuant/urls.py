"""UPQuant URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

import view
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
import WebQuant.views as web_view

urlpatterns = [
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url='static/WebQuant/images/favicon.ico')
    ),
    url('^demo/$', view.index),
    url('^index/$', web_view.index),
    url(r'^login/$', web_view.login),
    url(r'^loginjude/$', web_view.loginjude),
    url(r'^user_register/$', web_view.user_register),
    url(r'^register/$', web_view.register),
    url(r'^logout/$', web_view.logout),
    url('^strategy/$', web_view.strategy),
    url('^management/$', web_view.management),
    url('^college/$', web_view.college),
    url('^research_tmp/$', web_view.research),
    url('^database/$', web_view.database),
    url('^deal/$', web_view.deal),
    url('^deal/del/$', web_view.deal_del),
    url('^deal/stop/$', web_view.deal_stop),
    url('^deal/new/$', web_view.deal_new),
    url('^deal/new/this/$', web_view.deal_new_this),
    url('^deal/back-test/$', web_view.deal_back_test),
    url('^deal/daily/$', web_view.deal_daily),
    url('^deal/change_name/$', web_view.change_deal_name),
    url('^deal/log/$', web_view.deal_log),
    url('^deal/code/$', web_view.deal_code),
    url('^deal/check_deal/$', web_view.deal_check_deal),
    url('^deal/trade-detail/$', web_view.deal_trade_detail),
    url('^deal/chart_info/$', web_view.deal_back_test_chart),
    url('^deal/strategy_status/$', web_view.deal_strategy_status),
    url('^strategy/edit/$', web_view.strategy_edit),
    url('^strategy/history/$', web_view.history),
    url('^strategy/back-test/$', web_view.back_test),
    url('^strategy/trade-detail/$', web_view.trade_detail),
    url('^strategy/daily/$', web_view.daily),
    url('^strategy/log/$', web_view.log),
    url('^strategy/earnings/$', web_view.earnings),
    url('^strategy/alpha/$', web_view.alpha),
    url('^strategy/beta/$', web_view.beta),
    url('^strategy/sharpe/$', web_view.sharpe),
    url('^strategy/sortino/$', web_view.sortino),
    url('^strategy/inforatio/$', web_view.inforatio),
    url('^strategy/volatility/$', web_view.volatility),
    url('^strategy/maxdrawdown/$', web_view.maxdrawdown),
    url('^help/api/class/list/$', web_view.help_api),
    url(r'^admin/', admin.site.urls),
    url(r'^$', web_view.index),
]

urlpatterns += staticfiles_urlpatterns()

urlpatterns += format_suffix_patterns([
    url(r'^api/$', view.api_root),

    url(r'^api/strategynew',
        view.StrategyNew.as_view(),
        name='strategynew'),

    url(r'^api/strategycopy',
        view.StrategyCopyView.as_view(),
        name='strategycopy'),

    url(r'^api/strategydelete',
        view.StrategyDelete.as_view(),
        name='strategydelete'),

    url(r'^api/strategylist',
        view.StrategyListView.as_view(),
        name='strategylist'),

    url(r'^api/strategybacktest',
        view.StrategyBacktestView.as_view(),
        name='strategybacktest'),

    url(r'^api/strategysave',
        view.StrategySaveView.as_view(),
        name='strategysave'),

    url(r'^api/strategyinforatio',
        view.StrategyInfoRatioView.as_view(),
        name='strategyinforatio'),

    url(r'^api/strategyinfo',
        view.StrategyInfoView.as_view(),
        name='strategyinfo'),

    url(r'^api/returninfo',
        view.ReturnInfoView.as_view(),
        name='returninfo'),

    url(r'^api/tradedetail',
        view.TradeDetailView.as_view(),
        name='tradedetail'),

    url(r'^api/dailyposition',
        view.DailyPositionView.as_view(),
        name='dailyposition'),

    url(r'^api/outputlog',
        view.OutputLogView.as_view(),
        name='outputlog'),

    url(r'^api/strategyreturn',
        view.StrategyReturnView.as_view(),
        name='strategyreturn'),

    url(r'^api/strategyalpha',
        view.StrategyAlphaView.as_view(),
        name='strategyalpha'),

    url(r'^api/strategybeta',
        view.StrategyBetaView.as_view(),
        name='strategybeta'),

    url(r'^api/strategysharp',
        view.StrategySharpView.as_view(),
        name='strategysharp'),

    url(r'^api/strategysortino',
        view.StrategySortinoView.as_view(),
        name='strategysortino'),

    url(r'^api/strategyvolatility',
        view.StrategyVolatilityView.as_view(),
        name='strategyvolatility'),

    url(r'^api/strategymaxdrewdown',
        view.StrategyMaxDrewdownView.as_view(),
        name='strategymaxdrewdown'),

    url(r'^api/backtesthistory',
        view.BackTestHistoryView.as_view(),
        name='backtesthistory'),

    url(r'^api/codehistory',
        view.HistoryCodeView.as_view(),
        name='historycode'),

    url(r'^api/resulthistory',
        view.HistoryResultView.as_view(),
        name='historyresult'),
    
    url(r'^api/git/push_cb',
        view.GitView.as_view(),
        name='push_cb'),
])
