import Database.models as model
from rest_framework import serializers


class StrategyInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.UpQuantBktestSumInfo
        fields = ('runtime', 'initcash', 'begintime', 'endtime')


class ReturnInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.UpQuantBktestSumInfo
        fields = ('benchmark', 'totalreturn', 'myannualreturn', 'benchmarkannualreturn',
                  'alpha', 'beta', 'sharpe', 'inforatio',
                  'sortino', 'volatility', 'maxdrewdown', 'trackingerror', 'downsiderisk')


class DailyPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.UpQuantTradeinfo
        fields = ('orderdate', 'ordertime', 'stockcode', 'ordercount')

