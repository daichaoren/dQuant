Mustache.tags = ['[[', ']]']

var originalDrawPoints = Highcharts.seriesTypes.column.prototype.drawPoints;
        Highcharts.seriesTypes.column.prototype.drawPoints = function () {
            var merge  = Highcharts.merge,
                series = this,
                chart  = this.chart,
                points = series.points,
                i      = points.length;

            while (i--) {
                var candlePoint = chart.series[0].points[i];
                if(candlePoint.open != undefined && candlePoint.close !=  undefined){  //如果是K线图 改变矩形条颜色，否则不变
                    var color = (candlePoint.open < candlePoint.close) ? '#DD2200' : '#33AA11';
                    var seriesPointAttr = merge(series.pointAttr);
                    seriesPointAttr[''].fill = color;
                    seriesPointAttr.hover.fill = Highcharts.Color(color).brighten(0.3).get();
                    seriesPointAttr.select.fill = color;
                }else{
                    var seriesPointAttr = merge(series.pointAttr);
                }

                points[i].pointAttr = seriesPointAttr;
            }

            originalDrawPoints.call(this);
        }
	Highcharts.setOptions({
    lang:{
           contextButtonTitle:"图表导出菜单",
           decimalPoint:".",
           downloadJPEG:"下载JPEG图片",
           downloadPDF:"下载PDF文件",
           downloadPNG:"下载PNG文件",
           downloadSVG:"下载SVG文件",
           drillUpText:"返回 {series.name}",
           loading:"加载中",
           months:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
           noData:"没有数据",
           numericSymbols: [ "千" , "兆" , "G" , "T" , "P" , "E"],
           printChart:"打印图表",
           resetZoom:"恢复缩放",
           resetZoomTitle:"恢复图表",
           shortMonths: [ "一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
           thousandsSep:",",
           weekdays: ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期天"],
           rangeSelectorZoom:'缩放'
        }
    });
	function haha(dstr){
	var dstr = dstr.substring(0,4)+"/"+dstr.substring(4,6)+"/"+dstr.substring(6,8)+" 08:0:0";
	var dstr = new Date(dstr);
	return dstr.getTime();
}
		$(function () {
   $.post("/api/tradedetail", {

		}, function (data) {
        // split the data set into ohlc and volume
        var ohlc = [],
            volume = [],
            dataLength = data.klineDataList.length,
            // set the allowed units for data grouping
            groupingUnits = [[
                'week',                         // unit name
                [1]                             // allowed multiples
            ], [
                'month',
                [1, 2, 3, 4, 6]
            ]],
            i = dataLength-1;
        for (i; i > 0; i --) {
            ohlc.push([
                haha(data.klineDataList[i].Date), // the date
                data.klineDataList[i].open, // open
                data.klineDataList[i].high, // high
                data.klineDataList[i].low, // low
                data.klineDataList[i].close // close
            ]);
            volume.push([
                haha(data.klineDataList[i].Date), // the date
                data.klineDataList[i].volume // the volume
            ]);
        }
        // create the chart
        $('#container').highcharts('StockChart', {
            rangeSelector: {
                selected: 1
            },
            navigator: {
                enabled: false
            },
            title: {
                text: ''
            },
            exporting:{
            	enabled:false
            },
            credits: {
             enabled:false
        	},
            xAxis: {
                dateTimeLabelFormats: {
					second: "%Y-%m-%d<br/>%H:%M:%S",
					minute: "%Y-%m-%d<br/>%H:%M",
					hour: "%Y-%m-%d<br/>%H:%M",
					day: "%Y<br/>%m-%d",
					week: "%Y<br/>%m-%d",
					month: "%Y-%m",
					year: "%Y"
				},
            gridLineWidth: 1 ,//纵向网格线宽度
	        },
	        plotOptions: {
	            //修改蜡烛颜色
	            candlestick: {
	                color: '#33AA11',
	                upColor: '#DD2200',
	                lineColor: '#33AA11',
	                upLineColor: '#DD2200'
	            }
	        },
            yAxis: [{

                height: '60%',
                lineWidth: 2
            }, {

                top: '65%',
                height: '35%',
                offset: 0,
                lineWidth: 2
            }],
            series: [{
                type: 'candlestick',

                data: ohlc,
                dataGrouping: {

                    // units: groupingUnits
                }
            }, {
                type: 'column',

                data: volume,
                yAxis: 1,
                dataGrouping: {

                }
            }]
        });
    });
});

$.post("/api/strategyinfo",{

	},function(data){
		$(".right_cont .moudle_top .date").text(data.begintime+ " -- "+data.endtime);
		$(".right_cont .moudle_top .time").text(data.runtime+ "秒");
		$(".right_cont .moudle_top .initial").text(data.initcash);
});

$.post("/api/tradedetail",{
},function(data){
    console.log(data)
	for(var i =0 ;i<data.buySellPointList.length;i++){

		if(data.buySellPointList[i].orderstate>0){

			data.buySellPointList[i].orderstate = '<div class="tables_d4 " id="c_red">买入</div>';
	 	}else{

			data.buySellPointList[i].orderstate = '<div class="tables_d4 " id="c_green">卖出</div>';
	 	}

	}
	$('#transaction').text(data.transaction + " transaction");
	$('#c_orange').text("￥" + data.buy_vol.toFixed(2));
	$('#c_blue').text("￥" + data.sell_vol.toFixed(2));


	var template = $('#template').html();
	Mustache.parse(template);
	var rendered = Mustache.render(template, data);
	$('.table_data').html(rendered);
})