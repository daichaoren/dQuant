var flag = 1;

//初始化交易名称
function formatDate(num){
    return num<10?"0"+num:num
}
var d = new Date();
var mon = d.getMonth()+1;
$(".nick").val("模拟交易"+d.getFullYear() + "-" +formatDate(mon) + "-" + formatDate(d.getDate()) + " "+ formatDate(d.getHours()) + ":" + formatDate(d.getMinutes()) + ":" + formatDate(d.getSeconds()));

$(".choose").on('click', function(e) {
    e.preventDefault();
    if($('.listborder .listcont').length > 0){
        $(".pannel2").show();
        $(".mask").show();
    }else{
        $(".errtip2").text('完成一次回测后才能进行模拟交易!')
        $(".errtip2").slideDown();
    }
    

});
$(".creatpanel .nick").change(function(){
   
    var len = $(this).val().replace(/[^\x00-\xff]/g, "**").length;
    if(len <= 0  ){
        $(".errtip1").text('名称不能为空!');
        $(".errtip1").slideDown();
    }
    else if(len > 40){
        $(".errtip1").text('名称不超过40个字符!')
        $(".errtip1").slideDown();
    }else{
        $(".errtip1").slideUp();
    }
})
$(".creatpanel .initsum").change(function(){

    var len = parseInt($(this).val());
    if(len < 1  ){
        $(".errtip3").text('初始资金不少于1元!');
        $(".errtip3").slideDown();
    }
    else if(len > 1000000000){
        $(".errtip3").text('初始资金不超过1000000000元!')
        $(".errtip3").slideDown();
    }else{
        $(".errtip3").slideUp();
    }
})
$(".lchead").on('click',function(){
    if($(this).siblings('.listborder_bottom').text() != ''){
        $(this).siblings('.listborder_bottom').slideToggle();
        if(flag){
            $(this).children('i').css('transform','rotate(180deg)');
            flag=0;
        }else{
            $(this).children('i').css('transform','rotate(0)');
            flag=1;
        }
    }
})
$('label').click(function(){
    $(this).parents(".listborder").find("label").removeAttr('class');
    $(this).parents(".listborder").find("input").removeAttr('checked');
    $(this).attr('class', 'checked');
    $(this).prev().attr('checked', 'checked');
});
$(".no").click(function() {
    $(".pannel2").hide();
    $(".mask").hide();

});

var strategy_id = "";
var version_id = "";
// 选择策略
function select_strategy(){
    //获得选择的策略
    version_id = $('input[checked="checked"]').parents('li').attr('version_id');
	strategy_id = $('input[checked="checked"]').parents('.listborder_bottom').siblings().attr('strategy_id');
	$('.clnick').text($('input[checked="checked"]').parents('.listborder_bottom').siblings().children(".col1").text());

    $(".pannel2").hide();
    $(".mask").hide();
}

//运行策略
function on_run_deal(){
    deal_name = $(".nick").val();
    init_cash = $(".initsum").val();
    if( deal_name == '' || init_cash == '' || strategy_id == '' || $(".initsum").val() > 1000000000 || $(".initsum").val() < 1){
        $(".errtip4").text('请检查名称、策略、初始资金是否正确输入!')
        $(".errtip4").slideDown();
    }else{
        json_str = JSON.stringify({'deal_name': deal_name, 'strategy_id': strategy_id, 'version_id': version_id, "init_cash": init_cash });
        $.ajax({
            url: "/deal/new/",
            type: 'POST',
            data: json_str,
            dataType: 'json',
            success: function(data){
               window.location.href = '/deal/back-test/'
            },
        });
    }
    
}