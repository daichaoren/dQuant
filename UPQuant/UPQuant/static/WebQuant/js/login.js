$(function(){
	
	$('#switch_qlogin').click(function(){
		
		$('#switch_login').removeClass("switch_btn_focus").addClass('switch_btn');
		$('#switch_qlogin').removeClass("switch_btn").addClass('switch_btn_focus');
		$('#switch_bottom').animate({left:'0px',width:'70px'});
		$('#qlogin').css('display','none');
		$('#web_login').css('display','block');
		
		});
	$('#switch_login').click(function(){		
		
		$('#switch_login').removeClass("switch_btn").addClass('switch_btn_focus');
		$('#switch_qlogin').removeClass("switch_btn_focus").addClass('switch_btn');
		$('#switch_bottom').animate({left:'154px',width:'70px'});
		
		$('#qlogin').css('display','block');
		$('#web_login').css('display','none');
		});
        if(getParam("a")=='0')
        {
            $('#switch_login').trigger('click');
        }
	});
	
function logintab(){
	//scrollTo(0);
    $('#switch_login').removeClass("switch_btn_focus").addClass('switch_btn');
    $('#switch_qlogin').removeClass("switch_btn").addClass('switch_btn_focus');
    $('#switch_bottom').animate({left:'0px',width:'70px'});
    $('#qlogin').css('display','none');
    $('#web_login').css('display','block');
	
}

function registertab(){
    $('#switch_login').removeClass("switch_btn").addClass('switch_btn_focus');
    $('#switch_qlogin').removeClass("switch_btn_focus").addClass('switch_btn');
    $('#switch_bottom').animate({left:'154px',width:'70px'});

    $('#qlogin').css('display','block');
    $('#web_login').css('display','none');
}

//根据参数名获得该参数 pname等于想要的参数名
function getParam(pname) {
    var params = location.search.substr(1); // 获取参数 平且去掉？
    var ArrParam = params.split('&');
    if (ArrParam.length == 1) {
        //只有一个参数的情况
        return params.split('=')[1];
    }
    else {
         //多个参数参数的情况
        for (var i = 0; i < ArrParam.length; i++) {
            if (ArrParam[i].split('=')[0] == pname) {
                return ArrParam[i].split('=')[1];
            }
        }
    }
}


var reMethod = "GET",
	pwdmin = 6;

function login(){
    var  username = $('#u').val();
    var password = $('#p').val();
    if (username == ""){
        $('#userCue1').html("<font color='red'><b>×帐号不能为空</b></font>");
        return ;
    }
    if (password == "") {
            $('#userCue1').html("<font color='red'><b>×密码不能为空</b></font>");
        return ;
    }

    jQuery.ajax({
        url: "/loginjude/",
        type: 'POST',
        data: {"name":username, "password":password},
        success: function (result, statues, xml) {
            // console.info(result)
            if (result != null && result!="") {
                if(result == 1){
                    $('#userCue1').html("<font color='green'><b>√登录成功</b></font>");
                     setTimeout(function(){
                        window.location.href = "/index/";
                    },1000);

                }
                else{
                    $('#userCue1').html("<font color='red'><b>×账号或者密码错误</b></font>");
                    return ;
                }
            }
        }

    });
}
$(document).ready(function() {
	$('#log_in').click(function () {
        login();
	});
    document.onkeydown=function(event){
        var e = event || window.event || arguments.callee.caller.arguments[0];
        if(e && e.keyCode==27){ // 按 Esc
            //要做的事情
          }
        if(e && e.keyCode==113){ // 按 F2
             //要做的事情
           }
         if(e && e.keyCode==13){ // enter 键
             //要做的事情
             login();
        }
    };
	$('#reg').click(function() {

		if ($('#user').val() == "") {
			$('#user').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			$('#userCue1').html("<font color='red'><b>×帐号不能为空</b></font>");
			return false;
		}



		if ($('#user').val().length < 4 || $('#user').val().length > 16) {

			$('#user').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			$('#userCue').html("<font color='red'><b>×帐号位4-16字符</b></font>");
			return false;

		}

		if ($('#passwd').val().length < pwdmin) {
			$('#passwd').focus();
			$('#userCue').html("<font color='red'><b>×密码不能小于" + pwdmin + "位</b></font>");
			return false;
		}
		if ($('#passwd2').val() != $('#passwd').val()) {
			$('#passwd2').focus();
			$('#userCue').html("<font color='red'><b>×两次密码不一致！</b></font>");
			return false;
		}
		var username = $('#user').val();
		var password1 = $('#passwd').val();
		var password2 = $('#passwd2').val();
		jQuery.ajax({
            url: "/user_register/",
            type: 'POST',
            data: {'username': username, 'password1': password1, 'password2': password2},
            success: function (result, statues, xml) {

                if (result != null && result!="") {
                    if(result == 1){
                    $('#userCue').html("<font color='green'><b>√帐号注册成功, 请登录！</b></font>");
                    setTimeout(function(){
                    logintab();
                    },1000);
                    }
                    else{
                        $('#userCue').html("<font color='red'><b>×用户名已经存在！</b></font>");
                        return;
                    }
                }
            }
        });
		
	});
});