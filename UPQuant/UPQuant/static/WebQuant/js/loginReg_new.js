
var loginAction=function (){
	$('#loginWrap').toggle();
	$('#regWrap').toggle();
}
var loginAction2=function (){
	$('.mainBody').hide();
	$('.getCertify').show();
}
var loginAction3=function (){
	$('.topTitle_wrap').hide();
	$('.checkUsername').show();
	$('.getCertify').hide();
	$('.checkUsername_body').show();
}

var acceptFileChange = function() {
	if(!$(".remenberPsd_btn[for=acceptFile].checked").length<1){
		$("#btnReg").attr("disabled",true);
		$("#btnReg").addClass('disabled');
		$("#btnReg").addClass('disabledBtn');
	}else{
		$('#btnReg').removeAttr("disabled");
		$('#btnReg').removeClass("disabled");
		$('#btnReg').removeClass("disabledBtn");
	}
}
// var test = function(){
// 	$.post("/users/loginForPC",{hqr:"wq3gfNjLhlvx/yqC6n5UKc0pvUKa8BwlT3YCmSQ3kJQv8f8/NYoF2sBCOzWecVbYWeMg8QYhOiSpjuH+vAzRPsCAY/vLsHP8Cb55d/pKeOM="
// 		,username:"Ohpc7b3xJAg="},function(d){
// 		console.info(d);
// 	})
// }


$(function (){
	$('.remenberPsd_btn').on('click',function (){		
		if($(this).hasClass('checked')){
			$(this).removeClass('checked');
			$(this).prev().prop('checked','false');
		}else{			
			$(this).addClass('checked');
			$(this).prev().prop('checked','true');
		}
	});
	/**
	 * login part
	 *
	 */
	$('#loginWrap  #logInput_psd').on('keyup', function (event) {
		if (event.keyCode == "13") {
			//回车执行查询
			$('#btnLogin').click();
		}
	});

	$("#loginWrap  #logInput_username").focus(function(event) {
		$("#loginWrap .userName_wrap .errorIcon").remove();
		$("#loginWrap .userName_wrap .rightIcon").remove();
	});

	$("#loginWrap  #logInput_psd").focus(function(event) {
		$("#loginWrap .password_wrap .errorIcon").remove();
		$("#loginWrap .password_wrap .rightIcon").remove();
	});
	$("#loginWrap #logInput_username").blur(function(event) {
		var name_ = $("#logInput_username").val();

		$("#loginWrap .userName_wrap .errorIcon").remove();
		$("#loginWrap .userName_wrap .rightIcon").remove();
		if(!name_||name_.length<1){
			$("#loginWrap  .userName_wrap .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			$("#loginWrap  .error_tip").html("请输入用户名")
			$("#loginWrap  .error_tip").show();
			return;
		}
		if(!name_.match(/^[a-zA-Z0-9_]{1,}$/)){
			$("#loginWrap  .userName_wrap .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			$("#loginWrap  .error_tip").html("6-18位数字、小写字母或下划线")
			$("#loginWrap  .error_tip").show();
			return;
		}else {
			$("#loginWrap .userName_wrap .userName_box").after('<img src="images/rightIcon.jpg" class="rightIcon" />')
			$("#loginWrap .error_tip").hide();
            return;
		}
	});

	$("#loginWrap #btnLogin").on("click",function(){
		var jsonData_ = {};
		jsonData_.password=$("#loginWrap #logInput_psd").val();
		jsonData_.name = $("#loginWrap #logInput_username").val();
		if($("#loginWrap .errorIcon").length>0){
			return;
		}
		if(!jsonData_.password||jsonData_.password.length<1){
			$("#loginWrap  .password_wrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			$("#loginWrap  .error_tip").html("密码为6-18个字符")
			$("#loginWrap  .error_tip").show();
			return;
		}

		jQuery.ajax({
			url: "/users/login",
			type: 'POST',
			data: jsonData_,
			success: function (result) {
				$("#loginWrap .userName_wrap .errorIcon").remove();
				$("#loginWrap .userName_wrap .rightIcon").remove();
				$("#loginWrap .password_wrap .errorIcon").remove();
				$("#loginWrap .password_wrap .rightIcon").remove();
				// console.info(result)
				if (result != null && result!="") {
					if(result.resultCode==null){
						$("#loginWrap .error_tip").hide();
						if(result.codes) {
							$.cookie('codes', JSON.stringify(result.codes), { expires: 7, path: '/' });
						}
						callbackUrl();
					}
					else{
						if(result.resultCode=="3051"){
							$("#loginWrap .error_tip").html("用户名不存在，请重试")
							$("#loginWrap .error_tip").show();
							$("#loginWrap .password_wrap .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
                            return;
						}
						if(result.resultCode=="3011"){
							$("#loginWrap .error_tip").html("密码不匹配，请重试")
							$("#loginWrap .error_tip").show();
							$("#loginWrap .password_wrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
                            return;
						}
					}
				}
			}
		});
	});


	/**
	 *  register part
     */
	var phoneExistsFlag = false;
	$("#regWrap #phoneNum_userName").focus(function(){
		$("#regWrap .userName_wrap .errorIcon").remove();
		$("#regWrap .userName_wrap .rightIcon").remove();
	})

	$("#regWrap #phoneNum_userName").blur(function(){
		if($("#regWrap #phoneNum_userName").val().length<1){
			$("#regWrap .error_tip").html("请输入帐号")
			$("#regWrap .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
            return;
		}
		
	})

	$("#regWrap #phoneNum_psd").focus(function(){
		$("#regWrap .password_wrap .errorIcon").remove();
		$("#regWrap .password_wrap .rightIcon").remove();
	})

	$("#regWrap  #phoneNum_psd").blur(function(event) {
		var password = $("#regWrap  #phoneNum_psd").val();
		if(password.length<1){
			$("#regWrap .error_tip").html("请输入密码")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
            return;
		}
		if(password.length >= 6 && password.length <= 18){
			$("#regWrap .error_tip").html("")
			$("#regWrap .password_box").after('<img src="images/rightIcon.jpg" class="rightIcon" />')
            return;
		}else{
			$("#regWrap .error_tip").html("密码为6-18个字符")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
            return;
		}
	});


	var jsonData = {};
	$('#nextBtn').click(function(){
		jsonData.password=$("#phoneNum_psd").val();
		jsonData.phone = $("#phoneNum_userName").val();
		// jsonData.vCode = $("#certifyCode").val();

		$("#regWrap .error_tip").html("");
		$("#regWrap .rightIcon").remove();
		$("#regWrap .errorIcon").remove();

		if(jsonData.phone.length<1){
			$("#regWrap .error_tip").html("请输入帐号")
			$("#regWrap .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
            return;
		}
		
		if(jsonData.password.length<1){
			$("#regWrap .error_tip").html("请输入密码")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
            return;
		}
		if(jsonData.password ==""||jsonData.password.length < 6 || jsonData.password.length > 18){
			$("#regWrap .error_tip").html("密码为6-18个字符")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}
		
		console.log(jsonData);

		//判断手机号是否存在
		
		
	});
	

	$("#btnReg").click(function(){
		// var dis = $("#btnReg").attr("disabled");
		if(dis == "disabled"){
			return;
		}
		$("#regWrap .error_tip_mCode").html("");
		$("#regWrap .rightIcon").remove();
		$("#regWrap .errorIcon").remove();
		
		
		
	});

	

	function login(){
		jsonData.name = jsonData.phone;
		jQuery.ajax({
			url: "/users/login",
			type: 'POST',
			data: jsonData,
			success: function (result) {
				callbackUrl();
			}
		});
	}
	


});

// Reset password
