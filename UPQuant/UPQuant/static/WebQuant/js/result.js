Mustache.tags = ['[[', ']]']

function mcalc(data,m,i){
    var f = 0;
    while(m>=0){
        f += parseFloat(data[i-m].context);
        m--;
    }
    return f.toFixed(3);
}

function ajaxpost_result(url, index, init){
    $.post(url,{
        index: index
    },
    function(data){
        for(var i =0 ;i<data.returnList.length;i++){
            data.returnList[i].context = parseFloat(data.returnList[i].context).toFixed(3);
        }
        var template = $('#template').html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, data);
        $('.table_data').html(rendered);

	    for(i = 0 ;i<data.returnList.length;i++){
            if(i==1){
                // console.log(typeof parseFloat(data.returnList[0].context));
                $(".table_data li").eq(i).children("div").eq(2).text(mcalc(data.returnList,1,i));

            }
            else if(i>=2 && i<5){
                $(".table_data li").eq(i).children("div").eq(2).text(mcalc(data.returnList,1,i));
                $(".table_data li").eq(i).children("div").eq(3).text(mcalc(data.returnList,2,i));
            }

            else if(i>=5 && i<11){
                $(".table_data li").eq(i).children("div").eq(2).text(mcalc(data.returnList,1,i));
                $(".table_data li").eq(i).children("div").eq(3).text(mcalc(data.returnList,2,i));
                $(".table_data li").eq(i).children("div").eq(4).text(mcalc(data.returnList,5,i));

            }
            else if(i>=11){
                $(".table_data li").eq(i).children("div").eq(2).text(mcalc(data.returnList,1,i));
                $(".table_data li").eq(i).children("div").eq(3).text(mcalc(data.returnList,2,i));
                $(".table_data li").eq(i).children("div").eq(4).text(mcalc(data.returnList,5,i));
                $(".table_data li").eq(i).children("div").eq(5).text(mcalc(data.returnList,11,i));
            }
            else{

            }
        }

        if (init == 1) {
            $(".page p").createPage({
              pageCount:data.pageCount,
              current:index,
              backFn:function(p){
                  ajaxpost_result(url, p, 0);
              }
            });
        }
    });
}

function get_result(url)
{
    ajaxpost_result(url, 1, 1)
}

