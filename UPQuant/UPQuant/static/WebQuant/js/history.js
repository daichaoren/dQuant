Mustache.tags = ['[[', ']]']

var editor = ace.edit("editor1");
editor.setTheme("ace/theme/xcode");
editor.session.setMode("ace/mode/python");

Math.mul = function(v1, v2)
{
    var m = 0;
    var s1 = v1.toString();
    var s2 = v2.toString();
    try
    {
        m += s1.split(".")[1].length;
    }
    catch (e)
    {
    }
    try
    {
        m += s2.split(".")[1].length;
    }
    catch (e)
    {
    }

  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}

$(".table_data").on('click', '.chakan', function(e) {
    var versionid = $(this).parent().siblings(".vid").text();
    console.log(versionid);
    $.post("/api/codehistory",{
        "versionId":versionid
    },function(data){
        editor.session.setValue(data["codeContext"]);
        editor.setReadOnly(true);
        $(".panelbox").show();
    $(".windowmask").show();
    })
    e.preventDefault();
});

$(".table_data").on('click', '.jieguo', function(e) {
    var versionid = $(this).parent().siblings(".vid").text();
    $.post("/api/resulthistory",{
        "versionId":versionid
    },function(data){
       if (data.state == "1"){
            window.location.href = '/strategy/back-test'
       }
    })
});
$(".panelbox .panelclose").click(function() {
    $(".panelbox").hide();
    $(".windowmask").hide();
});


function ajax_post_history(index, init, beginTime, endTime){
    $.post("/api/backtesthistory",{
    "beginTime": beginTime,
    "endTime": endTime,
    "index": index
    },function(data){
        console.log(data);
        for(var i =0 ;i<data.backTestList.length;i++){
            if(data.backTestList[i].totalReturn>0){
                data.backTestList[i].totalReturn = "<div class='tables_d6' id='c_red'>"+Math.mul(data.backTestList[i].totalReturn,100).toFixed(2)+"%</div>";
            }
            else if(data.backTestList[i].totalReturn == 0){
                data.backTestList[i].totalReturn = "<div class='tables_d6' >"+Math.mul(data.backTestList[i].totalReturn,100).toFixed(2)+"%</div>";
            }
            else{
                data.backTestList[i].totalReturn = "<div class='tables_d6' id='c_green'>"+Math.mul(data.backTestList[i].totalReturn,100).toFixed(2)+"%</div>";
            }
           }
        var template = $('#template').html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, data);
        $('.table_data').html(rendered);

         if (init == 1) {
            $(".page p").createPage({
              pageCount:data.pageCount,
              current:index,
              backFn:function(p){
                  ajax_post_history(p, 0, beginTime, endTime);
              }
            });
        }
    });
}

var begin = $("#starttime").val();
var end = $("#endtime").val();
ajax_post_history(1, 1, "0", "0")

function search_back_test(){
    var begin = $("#starttime").val();
    var end = $("#endtime").val();
    ajax_post_history(1, 1, begin, end)
}

function all_back_test(){
  ajax_post_history(1, 1, "0", "0")
}

var picker = new Pikaday(
{
    field: document.getElementById('starttime'),
    firstDay: 1,
    minDate: new Date('2010/01/01'),
    maxDate: new Date('2020/12/31'),
    yearRange: [2000,2020]
});
 // picker.toString('YYYY/MM/DD');
 var picker1 = new Pikaday(
{
    field: document.getElementById('endtime'),
    firstDay: 1,
    minDate: new Date('2010/01/01'),
    maxDate: new Date('2020/12/31'),
    yearRange: [2000,2020]
});