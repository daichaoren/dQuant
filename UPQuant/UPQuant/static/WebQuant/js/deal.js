var checknum_stop = 0; //停止
var checknum_run = 0;  //运行
var checknum = 0;
$('.setall').on('click',function(){
  
    if($(this).is(':checked')){

        $('.query input').prop("checked", true)

        checknum = $('.query input').length;
        checknum_stop = $("li[status='已终止']").length;
        checknum_run = $("li[status='运行中']").length;
    }else{
        $('.query input').prop("checked",false);

        checknum = 0;checknum_stop=0;checknum_run=0;
    }
    cknum(0)
})
$(".delete").on('click', function(e) {
    e.preventDefault();
    $(".pannel1").show();
    $(".mask").show();

});
$(".no").click(function() {
    $(".pannel1").hide();
    $(".mask").hide();

});

function cknum(i){
    if(i == 0){
        if(checknum || checknum_run || checknum_stop){
             $('.delete').show();
        }else{
            $('.delete').hide();
        }
        
        if(checknum != checknum_run + checknum_stop){
            console.log(1);
            $('.setall').prop("checked", false)
        }
    }
    else if(i == 1){
        if(checknum_run){
            $('.delete').show();
        }else{
            $('.delete').hide();
        }
    }else{
        if(checknum_stop){
            $('.delete').show();
        }else{
            $('.delete').hide();
        }
    }
}

$('.query input[type=checkbox]').click(function(){
    if($(this).is(':checked')){

        if($(this).parents('li').attr("status") == "已终止"){
            checknum_stop += 1;
        }else{
            checknum_run += 1;
        }
        
    }else{

        if($(this).parents('li').attr("status") == "已终止"){
            checknum_stop -= 1;
        }else{
            checknum_run -= 1;
        }
    }
    cknum(0);
})


//点击事件

function show_all(){
    $('#display_all').attr("class", "on");
    $('#display_stop').attr("class", "");
    $('#display_run').attr("class", "");
    $("li[deal='true']").each(function(){
        $(this).show();
    });
}

function show_stop(){
    $('#display_all').attr("class", "");
    $('#display_stop').attr("class", "on");
    $('#display_run').attr("class", "");
    var i = 0;
    $("li[deal='true']").each(function(){
        var status = $(this).attr("status");
        if (status == "已终止"){
            $(this).show();
            i ++;
        }
        else{
           $(this).hide();
        }
    });

    if (i == 0){
         $(".hascont").hide();
         $(".nocont").show();
    }
     else{
        $(".hascont").show();
         $(".nocont").hide();
    }
    cknum(2);
}

function show_run(){
   $('#display_all').attr("class", "");
    $('#display_stop').attr("class", "");
    $('#display_run').attr("class", "on");
    var i = 0;
    $("li[deal='true']").each(function(){
        var status = $(this).attr("status");
        if (status == "运行中"){
            $(this).show();
            i ++;
        }
        else{
           $(this).hide();
        }
    });

    if (i == 0){
         $(".hascont").hide();
         $(".nocont").show();
    }
    else{
        $(".hascont").show();
         $(".nocont").hide();
    }
    cknum(1);
}

// 删除仿真交易
function delete_deal(){

    var delete_list = [];
     $("li[deal='true']").each(function(){
        var checkbox = $(this).find('input');
        if(checkbox.prop('checked') == true){
           delete_list.push($(this).attr("deal_id"));
           $(this).remove();
        }
    });
    json_str = JSON.stringify({'delete_list': delete_list});
    $.ajax({
        url: "/deal/del/",
        type: 'POST',
        data: json_str,
        dataType: 'json',
        success: function(data){
            $('.pannel1').hide();
            $(".mask").hide();
        },
    });
}

//进入交易
function on_goto_deal(obj){
    deal_id = $(obj).attr('deal_id');
    deal_name = $(obj).text();

    json_str = JSON.stringify({'deal_id': deal_id, 'deal_name': deal_name});

    $.ajax({
        url: "/deal/back-test/",
        type: 'POST',
        data: json_str,
        dataType: 'json',
        success: function(data){
            window.location.href='/deal/back-test/';
        },
    });

}