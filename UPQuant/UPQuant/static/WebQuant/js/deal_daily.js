
//分页处理
Mustache.tags = ['[[', ']]']

function ajaxpost_daily(index, init){
    $.post("/deal/daily/",{
    index: index
    },function(data){
        data = JSON.parse(data);
        console.log(data.returnList);
        for(var i =0 ;i<data.returnList.length;i++){

            data.returnList[i].return = parseFloat(data.returnList[i].return).toFixed(3);
            if(parseFloat(data.returnList[i].positionValue)>0){

                data.returnList[i].positionValue = '<div class="tables_d4" id="c_red">￥'+data.returnList[i].positionValue +'</div>';
            }else{

                data.returnList[i].positionValue = '<div class="tables_d4" id="c_green">￥'+data.returnList[i].positionValue+'</div>';
            }
            if(parseFloat(data.returnList[i].return)>0){

                data.returnList[i].return = '<div class="tables_d5 " id="c_red">￥'+data.returnList[i].return+'</div>';
            }else{

                data.returnList[i].return = '<div class="tables_d5 " id="c_green">￥'+data.returnList[i].return+'</div>';
            }
        }
        var template = $('#template').html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, data);
        $('.table_data').html(rendered);
        if (init == 1) {
            $(".page p").createPage({
              pageCount:data.pageCount,
              current:index,
              backFn:function(p){
                  ajaxpost_daily(p, 0);
              }
            });
        }
    })
}

ajaxpost_daily(1, 1);