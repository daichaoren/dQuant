$(function(){
	$(".forgetPsd").click(function(event) {
		$("#forgetPsd").show();
		$("#loginWrap_reset").hide();
	});
	var PhoneExistsFlag = false;
	$("#forgetPsd #reset_userName").focus(function(){
		$("#forgetPsd .userName_wrap .errorIcon").remove();
		$("#forgetPsd .userName_wrap .rightIcon").remove();
	})

	$("#forgetPsd #reset_userName").blur(function(){
		if($("#forgetPsd #reset_userName").val().length<1){
			$("#forgetPsd .error_tip").html("请输入手机号")
			$("#forgetPsd .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}
		var telReg = !$("#forgetPsd #reset_userName").val().match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/);
		if(telReg){
			$("#forgetPsd .error_tip").html("请输入正确格式的手机号")
			$("#forgetPsd .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}else{

			$("#forgetPsd .error_tip").html("")
			$("#forgetPsd .userName_box").after('<img src="images/rightIcon.jpg" class="rightIcon" />')
			PhoneExistsFlag = true;
			return;

		}
	})




	var jsonData = {};
	$('#nextBtn').click(function(){

		jsonData.vCode = $("#reset_NumTxt").val();

		$("#forgetPsd .error_tip").html("");
		$("#forgetPsd .rightIcon").remove();
		$("#forgetPsd .errorIcon").remove();


		if(jsonData.vCode ==""){
			$("#forgetPsd .error_tip").html("请输入验证码")
			$("#forgetPsd .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}


		//判断手机号是否存在
		if(!phoneExistsFlag){
			return;
		}

		jQuery.ajax({
			url: "",
			type: 'POST',
			data: {},
			success: function (data) {
				$("#forgetPsd .error_tip").html("");
				$("#forgetPsd .rightIcon").remove();
				$("#forgetPsd .errorIcon").remove();
				if(data.result){
					$('#forgetPsd').hide();
					$('#forgetPsd_send').show();
					$("#reset_phoneNum").html(jsonData.phone);
					util.hsTime("#reset_resendWrap");
				}else{
					if(data.resultCode==""){
						$("#forgetPsd .error_tip").html("请输入正确的验证码")
						$("#forgetPsd .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd .error_tip").html("手机号已认证")
						$("#forgetPsd .userName_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd .error_tip").html("验证码已过期，请输入新的验证码")
						$("#forgetPsd .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd .error_tip").html("获取验证码次数过多")
						$("#forgetPsd .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd .error_tip").html("验证码错误，请重试")
						$("#forgetPsd .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
				}
			}
		});
	});
	var util = {
		wait: 100,
		hsTime: function (that) {
			_this = this;
			if (_this.wait == 0) {
				$('#reset_resendWrap').removeAttr("disabled").text('获取验证码');
				$('#breset_resendWrap').css("color","#f8935c");
				$('#reset_resendWrap').css("border","1px solid #f8935c");
				_this.wait = 100;
			} else {
				var _this = this;
				$(that).attr("disabled", true).text('重新发送(' + _this.wait + ')');
				$(that).css("color","#c0c0c0");
				$('#reset_resendWrap').css("border","1px solid #c0c0c0");
				_this.wait--;
				if(_this.wait <= 0){
					_this.wait = 0;
				}
				setTimeout(function () {
					_this.hsTime(that);
				}, 1000)
			}
		}
	}
	//获取验证码
	$("#reset_resendWrap").click(function(){
		var dis = $("#reset_resendWrap").attr("disabled");
		if(dis == "disabled"){
			return;
		}
		jQuery.ajax({
			url: "",
			type: 'POST',
			data: {},
			success: function (data) {

				$("#forgetPsd_send .error_tip_mCode").html("");
				$("#forgetPsd_send .rightIcon").remove();
				$("#forgetPsd_send .errorIcon").remove();
				if(data.result){
					$("#forgetPsd_send .phoneCfCodeTxt_wrap").show()
					$("#forgetPsd_send .error_tip_mCode").hide()
					$("#reset_resendWrap").attr("disabled",true);
					util.hsTime("#reset_resendWrap");
				}else{
					$("#forgetPsd_send .phoneCfCodeTxt_wrap").hide()
					$("#forgetPsd_send .error_tip_mCode").show()
					if(data.resultCode==""){
						$("#forgetPsd_send .error_tip_mCode").html("验证码已过期，请输入新的验证码")
						$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd_send .error_tip_mCode").html("短时间内最多获取3次验证码，请稍后重试")
						$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
					if(data.resultCode==""){
						$("#forgetPsd_send .error_tip_mCode").html("验证码错误，请重试")
						$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
						return;
					}
				}
			}
		});
	});



	$("#reset_btn").click(function(){
		var dis = $("#reset_btn").attr("disabled");
		if(dis == "disabled"){
			return;
		}
		$("#forgetPsd_send .error_tip_mCode").html("");
		$("#forgetPsd_send .rightIcon").remove();
		$("#forgetPsd_send .errorIcon").remove();
		jsonData.mcode = $("#reset_certifyCode").val();
		jsonData.password=$("#new_psd").val();
		if(jsonData.mcode.length<1){
			$("#forgetPsd_send .phoneCfCodeTxt_wrap").hide()
			$("#forgetPsd_send .error_tip_mCode").show()
			$("#forgetPsd_send .error_tip_mCode").html("请输入短信验证码")
			$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}
		if(jsonData.password.length<1){
			$("#regWrap .error_tip").html("请输入密码")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}
		if(jsonData.password ==""||jsonData.password.length < 6 || jsonData.password.length > 18){
			$("#regWrap .error_tip").html("密码为6-18个字符")
			$("#regWrap .password_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
			return;
		}
		var param ='password='+jsonData.password+'&verifyCode='+jsonData.mcode;
		//验证短信验证码
		$.post("",param,function(data){
			if(data.result){
				$("#forgetPsd_send .phoneCfCodeTxt_wrap").show()
				$("#forgetPsd_send .error_tip_mCode").hide()
				$(".content").hide();
				window.open("");

			}else{
				$("#forgetPsd_send .phoneCfCodeTxt_wrap").hide()
				$("#forgetPsd_send .error_tip_mCode").show()
				if(data.resultCode==""){
					$("#forgetPsd_send .error_tip_mCode").html("验证码已过期，请输入新的验证码")
					$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
					return;
				}
				if(data.resultCode==""){
					$("#forgetPsd_send .error_tip_mCode").html("短时间内最多获取3次验证码，请稍后重试")
					$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
					return;
				}
				if(data.resultCode==""){
					$("#forgetPsd_send .error_tip_mCode").html("验证码错误，请重试")
					$("#forgetPsd_send .certifyCode_box").after('<img src="images/errorIcon.jpg" class="errorIcon" />')
					return;
				}
			}
		})
	});

	$(".closeBtn_wrap .closeBtn").click(function(event) {
		$(".content").hide();

	});


});




