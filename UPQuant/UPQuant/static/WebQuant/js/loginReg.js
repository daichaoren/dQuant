$("#loginWrap #btnLogin").on("click",function(){
    var jsonData_ = {};
    jsonData_.password=$("#loginWrap #logInput_psd").val();
    jsonData_.name = $("#loginWrap #logInput_username").val();
    if($("#loginWrap .errorIcon").length>0){
        return;
    }
    if(!jsonData_.password||jsonData_.password.length<1){
        $("#loginWrap  .password_wrap .password_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
        $("#loginWrap  .error_tip").html("密码为6-18个字符")
        $("#loginWrap  .error_tip").show();
        return;
    }

    jQuery.ajax({
        url: "/loginjude/",
        type: 'POST',
        data: jsonData_,
        success: function (result, statues, xml) {
            $("#loginWrap .userName_wrap .errorIcon").remove();
            $("#loginWrap .userName_wrap .rightIcon").remove();
            $("#loginWrap .password_wrap .errorIcon").remove();
            $("#loginWrap .password_wrap .rightIcon").remove();
            // console.info(result)
            if (result != null && result!="") {
                if(result == 1){

                    window.location.href = "/index/";
                }
                else{
                    $("#loginWrap .error_tip").html("用户名或密码错误，请重试")
                    $("#loginWrap .error_tip").show();
                    $("#loginWrap .password_wrap .userName_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
                    return;
                }
            }
        }

    });
});


$('#registerBtn').click(function(){
    password1=$("#phoneNum_psd1").val();
    password2=$("#phoneNum_psd2").val();
    username = $("#phoneNum_userName").val();


    $("#regWrap .error_tip").html("");
    $("#regWrap .rightIcon").remove();
    $("#regWrap .errorIcon").remove();

    if(username.length<1){
        $("#regWrap .error_tip").html("请输入帐号")
        $("#regWrap .userName_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
        return;
    }

    if(password1.length<1){
        $("#regWrap .error_tip").html("请输入密码")
        $("#regWrap .password_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
        return;
    }
    if(password2 ==""||password2.length < 3 || password2.length > 18){
        $("#regWrap .error_tip").html("密码为3-18个字符")
        $("#regWrap .password_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
        return;
    }
    if (password1 != password2){
        $("#regWrap .error_tip").html("两次输入的密码不一致")
        $("#regWrap .password_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
        return;
    }
    jQuery.ajax({
        url: "/user_register/",
        type: 'POST',
        data: {'username': username, 'password1': password1, 'password2': password2},
        success: function (result, statues, xml) {
            $("#loginWrap .userName_wrap .errorIcon").remove();
            $("#loginWrap .userName_wrap .rightIcon").remove();
            $("#loginWrap .password_wrap .errorIcon").remove();
            $("#loginWrap .password_wrap .rightIcon").remove();
            // console.info(result)
            if (result != null && result!="") {
                if(result == 1){
                    alert("注册成功，请登录！");
                    window.location.href = "/login/";
                }
                else{
                    $("#loginWrap .error_tip").html("用户名或密码错误，请重试")
                    $("#loginWrap .error_tip").show();
                    $("#loginWrap .password_wrap .userName_box").after('<img src="/static/WebQuant/images/errorIcon.jpg" class="errorIcon" />')
                    return;
                }
            }
        }

    });
});

var loginAction=function (){
	$('#loginWrap').toggle();
	$('#regWrap').toggle();
}