
Mustache.tags = ['[[', ']]']

function placeholderSupport() {
	return 'placeholder' in document.createElement('input');
}

function formatDate(num){
    return num<10?"0"+num:num
}
var delStrategyId = "";
// 按钮功能
$(".head_right a").click(function() {
	var d = new Date();
	var mon = d.getMonth()+1;
	$(".pannel .ti").attr("placeholder","新建策略"+d.getFullYear() + "-" +formatDate(mon) + "-" + formatDate(d.getDate()) + " "+ formatDate(d.getHours()) + ":" + formatDate(d.getMinutes()) + ":" + formatDate(d.getSeconds()));
	// $(".pannel .ti").val("新建策略"+d.getFullYear() + "-" +formatDate(mon) + "-" + formatDate(d.getDate()) + " "
	// + formatDate(d.getHours()) + ":" + formatDate(d.getMinutes()) + ":" + formatDate(d.getSeconds()));
	if(!placeholderSupport()){   // 判断浏览器是否支持 placeholder
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
	};
	$(".pannel").show();
	$(".mask").show();

});

$(".target").on('click', '.delete', function(e) {
	e.preventDefault();
	$(".pannel1").show();
	$(".mask").show();
    delStrategyId = $(this).siblings(".session_strategyId").text();
});
// $(".delete").click(function(e) {
// 	e.preventDefault();
// 	$(".pannel1").show();
// 	$(".mask").show();

// });

$(".pannel .close").click(function() {
	$(".pannel").hide();
	$(".mask").hide();

});
$(".yes").click(function() {

    if (delStrategyId == ""){
        console.log("delete strategy id is null, please to check it.");
        $(".pannel1").hide();
        $(".mask").hide();
        return false;
    }
    $.post("/api/strategydelete", {
			"strategyId": delStrategyId
		},
		function(data){
		    $(".pannel1").hide();
	        $(".mask").hide();
            window.location.reload();
		});
});
$(".no").click(function() {
	$(".pannel1").hide();
	$(".mask").hide();

});
$("#stock").click(function(){

});
$("#futures,#store").click(function(e){
	e.preventDefault();
	$(".head_left li").removeClass("on");
	$(this).addClass("on");
	$(".page").hide();
	$(".target").html('<div style="height:700px;line-height:700px;text-align:center">敬请期待！</div>');
});
// 按钮交互

$(".no").mousedown(function(){
	$(this).css("background","#dcdcdc");
})
$(".no").mouseup(function(){
	$(this).css("background","#e5e5e5");
})
$(".no").mouseleave(function(){
	$(this).css("background","#e5e5e5");
})
$(".yes").mousedown(function(){
	$(this).css("background","#d04b3d");
})
$(".yes").mouseup(function(){
	$(this).css("background","#ed6051");
})
$(".yes").mouseleave(function(){
	$(this).css("background","#ed6051");
})
$(".con_right .btn").mousedown(function(){
	$(this).css("background","#eeeeee");
})
$(".con_right .btn").mouseup(function(){
	$(this).css("background","#fff");
})
$(".con_right .btn").mouseleave(function(){
	$(this).css("background","#fff");
})

function ajaxpost(index, init) {
    $.post("/api/strategylist", {
			index: index,
		},
		function(data){
		    //alert(data);
			//data = JSON.parse(data);
			if (data == null || data == undefined || data == "")
			    return ;
			console.log(data);
			for(var i =0 ;i<data.StrategyList.length;i++){

                if(data.StrategyList[i].earnings>0){
			         data.StrategyList[i].earnings = '<span id="c_red">'+(data.StrategyList[i].earnings*100).toFixed(2)+'%</span>';
			 	}
			 	else if(data.StrategyList[i].earnings == 0){
                    data.StrategyList[i].earnings = '<span >'+(data.StrategyList[i].earnings*100).toFixed(2)+'%</span>';
			 	}
			 	else{
			        data.StrategyList[i].earnings = '<span id="c_green">'+(data.StrategyList[i].earnings*100).toFixed(2)+'%</span>';
			 	}

			 	if (data.StrategyList[i].backtestHistory == 0){
                    data.StrategyList[i].backtestState = "<span class='tip'>回测未完成</span>";
			 	}
			 	else{
                    data.StrategyList[i].backtestState = "";
			 	}

			}
			var template = $('#template').html();
			Mustache.parse(template);
			var rendered = Mustache.render(template, data);
			$('.target').html(rendered);

            if (init == 1){
                $(".page p").createPage({
                  pageCount:data.pageCount,
                  current:index,
                  backFn:function(p){
                      ajaxpost(p, 0);
                  }
                });
            }
	},"json");
}

ajaxpost(1, 1);

function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();
    return currentdate;
}

function on_strategy_new()
{
    var strategyName = $('.pannel .ti').val()||$('.pannel .ti').attr("placeholder");
    var date = new Date();
    var createTime = getNowFormatDate();
    $.post("/api/strategynew", {
            strategyName: strategyName,
            createTime: getNowFormatDate(),
            modifyTime: getNowFormatDate(),
        },
        function(data){
            edit_url = "/strategy/edit/?strategyId=" + data.strategyid + "&versionId=" + data.versionid.toString();
            window.location.href = edit_url;
    },"json");
}
$('input[type=checkbox]').click(function(){
	if($(this).is(':checked')){  
		$(this).attr('checked',true).siblings().attr('checked',false);

	}else{
		$(this).attr('checked',false).siblings().attr('checked',false);
	}
})