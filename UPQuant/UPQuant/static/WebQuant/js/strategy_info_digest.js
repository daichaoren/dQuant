
//获得概要信息
$.post("/api/strategyinfo",{
    },
    function(data){
        $(".right_cont .moudle_top .date").text(data.begintime+ " -- "+data.endtime);
        $(".right_cont .moudle_top .time").text(data.runtime+ "秒");
        $(".right_cont .moudle_top .initial").text(data.initcash);
});


//获得模拟交易状态
$.ajax({
    url: "/deal/strategy_status/",
    type: 'GET',
    dataType: 'json',
    success: function(data){
       if (data.state == 0){
            $(".startdeal_btn").hide();
            $(".ondeal_btn").show();
       }
       else {
            $(".startdeal_btn").show();
            $(".ondeal_btn").hide();
       }
    },
});

//开始模拟按钮事件
function on_start_deal_btn(){
    var d = new Date();
    var mon = d.getMonth()+1;
    $(".nick").val("模拟交易"+d.getFullYear() + "-" +formatDate(mon) + "-" + formatDate(d.getDate()) + " "+ formatDate(d.getHours()) + ":" + formatDate(d.getMinutes()) + ":" + formatDate(d.getSeconds()));

     $(".mask").show();
     $(".panel").show();
}

//取消模拟
function cancel_deal(){
    $(".panel").hide();
    $(".mask").hide();

}
//初始化交易名称
function formatDate(num){
    return num<10?"0"+num:num
}
// 开始模拟按钮
function start_deal(){

    //开始模拟交易事件
    deal_name = $(".nick").val();
    init_cash = $(".initsum").val();
    if( deal_name == '' || init_cash == ''  || $(".initsum").val() > 1000000000 || $(".initsum").val() < 1){
        $(".errtip4").text('请检查名称、初始资金是否正确输入!')
        $(".errtip4").slideDown();
    }else{
        json_str = JSON.stringify({'deal_name': deal_name, "init_cash": init_cash });
        $.ajax({
            url: "/deal/new/this/",
            type: 'POST',
            data: json_str,
            dataType: 'json',
            success: function(data){
               //alert(data.state);
               window.location.href = '/deal/back-test/'
            },
        });
    }
    
}

//正在模拟交易
function check_deal(){
    $.ajax({
        url: "/deal/check_deal/",
        type: 'GET',
        dataType: 'json',
        success: function(data){
           console.log(data.state);
           window.location.href = '/deal/back-test/'
        },
    });

}
$(".panel .nick").change(function(){
   
    var len = $(this).val().replace(/[^\x00-\xff]/g, "**").length;
    if(len <= 0  ){
        $(".errtip1").text('名称不能为空!');
        $(".errtip1").slideDown();
    }
    else if(len > 40){
        $(".errtip1").text('名称不超过40个字符!')
        $(".errtip1").slideDown();
    }else{
        $(".errtip1").slideUp();
    }
})
$(".panel .initsum").change(function(){

    var len = parseInt($(this).val());
    if(len < 1  ){
        $(".errtip3").text('初始资金不少于1元!');
        $(".errtip3").slideDown();
    }
    else if(len > 1000000000){
        $(".errtip3").text('初始资金不超过1000000000元!')
        $(".errtip3").slideDown();
    }else{
        $(".errtip3").slideUp();
    }
})