# coding=utf-8

from django import template

# 这句是必须滴
register = template.Library()


# 这个类是用来处理Tag的Node的，逻辑很简单
class NavTagItem(template.Node):
    def __init__(self, nav_path, nav_displaytext):
        self.path = nav_path.strip('"')
        self.text = nav_displaytext.strip('"')

    def render(self, context):
        cur_path = context['request'].path
        # context['request']是views传入模板中的request对像，可以通过这种方法从上
        # 文对象context中取得
        current = False
        if self.path == '/strategy/back-test':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="income on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="income"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/trade-detail':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="detail on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="detail"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/daily':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="daily on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="daily"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/log':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="diary on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="diary"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/earnings':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="way on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="way"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/alpha':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="alpha on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="alpha"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/beta':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="beta on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="beta"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/sharpe':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="sharp on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="sharp"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/sortino':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="stn on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="stn"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/inforatio':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="msg on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="msg"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/volatility':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="wave on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="wave"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/maxdrawdown':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="max on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="max"><span>%s</span></li></a>' % (self.path, self.text)
        elif self.path == '/strategy/history':
            if cur_path.startswith(self.path):
                return '<a href="%s" ><li class="back on"><span>%s</span></li></a>' % (self.path, self.text)
            else:
                return '<a href="%s" ><li class="back"><span>%s</span></li></a>' % (self.path, self.text)

# 注册tag，函数基本就是这个样子，不怎么会有变化
@register.tag
def navtagitem(parser, token):
    try:
        tag_name, nav_path, nav_text = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, \
            "%r tag requires exactly two arguments: path and text" % \
            token.split_contents[0]

    return NavTagItem(nav_path, nav_text)
