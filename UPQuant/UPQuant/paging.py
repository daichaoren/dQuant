def get_page_count(result_list, count_per_page):
    page = len(result_list) / count_per_page
    if len(result_list) % count_per_page != 0:
        page += 1
    return page


def get_page_list(index, result_list, count_per_page):
    if len(result_list) == 0:
        return []
    split_list = [result_list[i: i+count_per_page] for i in range(0, len(result_list), count_per_page)]

    return split_list[index-1]
