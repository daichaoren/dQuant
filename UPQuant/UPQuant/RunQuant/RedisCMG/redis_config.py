# -*- coding: utf-8 -*-
'''
Created on 2016-8-5

@author: root
'''

class RedisConfig(object):
    '''
    classdocs
    '''


    def __init__(self, **params):
        '''
        Constructor
        '''
        self.host = params.get('host')
        self.port = params.get('port')
        self.db = params.get('db')
        self.password = params.get('password')

    def get_host(self):
        return self.__host


    def get_port(self):
        return self.__port


    def get_db(self):
        return self.__db


    def get_password(self):
        return self.__password


    def set_host(self, value):
        self.__host = value


    def set_port(self, value):
        self.__port = value


    def set_db(self, value):
        self.__db = value


    def set_password(self, value):
        self.__password = value
        
MARKTDATA_TO_RedisDB = {
    "XSHE":0,  #深圳市场
    "XSHG":0   #上海市场          
}   
