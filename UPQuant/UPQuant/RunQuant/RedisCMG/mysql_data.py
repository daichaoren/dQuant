'''
Created on Sep 9, 2016

@author: hw
'''
import pandas as pd
import MySQLdb
class MyqlDataReader(object):
    def __init__(self, **kwargs):
        self.engine = kwargs.get("engine", "mysql")
        self.db = kwargs.get("db")
        self.user = kwargs.get("user", "root")
        self.password = kwargs.get("passwd", "root")
        self.host = kwargs.get("host", "localhost")
        self.port = kwargs.get("port", 3306) 
        self.charset = kwargs.get("charset", "utf8")
        self.conn = MySQLdb.connect(host=self.host, user=self.user, passwd=self.password, port=self.port, db=self.db, charset=self.charset)
  
    def getDict(self):
        sql = "select * from stock_baseinfo " 
        df = pd.read_sql(sql, self.conn, index_col="code")
        print df
        
        
if __name__ == '__main__':
    DATABASES = {
        'hqdata': {
            'engine':'mysql',
            'db': 'HqData',
            'user': 'root',
            'password': 'zhw1519',
            'host': '192.168.7.188',
            'port': 3306
        }
    }
        
    print DATABASES["hqdata"]
    mysqReader = MyqlDataReader(host=DATABASES["hqdata"].get("host"), user=DATABASES["hqdata"].get("user"), passwd=DATABASES["hqdata"].get("password"), db=DATABASES["hqdata"].get("db"), charset="utf8")
#     dict.saveHisDayKline()
    mysqReader.getDict()       
