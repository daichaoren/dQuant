# -*- coding: utf-8 -*-
#
# Copyright 2016 Ricequant, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from enum import Enum
from _pytest.config import Config

ORDER_STATUS = Enum("ORDER_STATUS", [
    "OPEN",
    "FILLED",
    "REJECTED",
    "CANCELLED",
])

EVENT_TYPE = Enum("EVENT_TYPE", [
    "DAY_START",
    "HANDLE_BAR",
    "DAY_END",
])

EXECUTION_PHASE = Enum("EXECUTION_PHASE", [
    "INIT",
    "HANDLE_BAR",
    "BEFORE_TRADING",
    "SCHEDULED",
    "FINALIZED",
])

STDMARKT_TO_INNER = {
    "XSHE": 0,
    "XSHG": 1
}

DATA_FREQUENCY = ['1m', '5m', '15m', '30m', '60m', '1d']

# mysql database Config
DATABASES = {
    'hqdata': {
        'engine': 'mysql',
        'db': 'HqData',
        'user': 'root',
        'password': 'mysqlroot',
        'host': '127.0.01',
        'port': 3306
    }
}
# redis database Config
REDIS = {
    'hqdata': {
        'password': 'redisserver',
        'host': '127.0.01',
        'port': 6379
    }
}


class DAYS_CNT(object):
    DAYS_A_YEAR = 365
    TRADING_DAYS_A_YEAR = 252
