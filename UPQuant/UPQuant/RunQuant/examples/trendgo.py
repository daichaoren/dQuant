import talib
import numpy as np
import math
import pandas
# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):

    #选择我们感兴趣的股票
    context.s1 = "000001.XSHE"
    context.stocks = [context.s1]
    context.SHORTPERIOD = 12
    context.LONGPERIOD = 26
    context.SMOOTHPERIOD = 9
    context.OBSERVATION = 100
    
# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 开始编写你的主要的算法逻辑
    
    prices = history(context.OBSERVATION,'1d','close')[context.s1].values
    macd,signal,hist = talib.MACD(prices,context.SHORTPERIOD,context.LONGPERIOD,context.SMOOTHPERIOD)
    minPrices = history(context.OBSERVATION,'5m','close')[context.s1].values
    min5macd,min5signal,min5hist = talib.MACD(minPrices,context.SHORTPERIOD,context.LONGPERIOD,context.SMOOTHPERIOD)
#     print(minPrices)
    # macd 是长短均线的差值，signal是macd的均线，使用macd策略有几种不同的方法，我们这里采用macd线突破signal线的判断方法
    if macd[-1] > macd[-2] and macd[-2] > macd[-3]:
        if min5macd[0] < 0:
            if min5macd[-1] >  min5macd[-2] and min5macd[-2] > min5macd[-3]:
                order_target_value(context.s1,1)
 
      
    # 如果macd从上往下跌破macd_signal
    if macd[-1]-signal[-1]<0 and macd[-2]-signal[-2]>0:
        # 计算现在portfolio中股票的仓位
        curPosition = context.portfolio.positions[context.s1].quantity
        #进行清仓
        if curPosition>0:
            order_target_value(context.s1,0)
             
    # 如果短均线从下往上突破长均线，为入场信号
    if macd[-1]-signal[-1]>0 and macd[-2]-signal[-2]<0:
    #满仓入股
        order_target_percent(context.s1, 1)
