# coding=utf-8
'''
Created on 2016-11-29

@author: xiaochun
'''
from data.data_finance import FIN_IDX_ANA,COM_SHARE_STRU, DataCenterFinData

data_finance = DataCenterFinData()

fin_idx_ana = FIN_IDX_ANA
com_share_stru = COM_SHARE_STRU
__all__ = ['fin_idx_ana', 'com_share_stru']


def export_as_api(func):
    __all__.append(func.__name__)
    return func


@export_as_api
def get_data_finance():
    return data_finance


@export_as_api
def query(*entities, **kwargs):
    """
        通过sqlalchemy.orm  session.query函数获取Query对象
    Parameters
    ------
        *entities：
            所查数据库表的映射类
    return
    ------
        返回一个sqlalchemy.orm.query.Query对象
    """
    return get_data_finance().query(*entities, **kwargs)


@export_as_api
def get_fundamentals(query_Object, entry_date=None, interval=None):
    """
        获取财务数据
    Parameters
    ------
        query_Object:sqlalchemy.orm.query.Query
        entry_date:string
            开始日期 format : YYYY-MM-DD 为空时取当前系统日期
        interval:string
            财务数据的间隔。例如，填写'5y'，则代表从entry_date开始（包括entry_date）回溯5年，返回数据时间以年为间隔。'd'-天，'w'-周，'m'-月，‘q’-季，'y'-年
    return
    ------
        pandas.DataFrame
            每一行对应数据库返回的每一行，列索引是你查询的所有字段
    """
    return get_data_finance().get_fundamentals(query_Object, entry_date, interval)


if __name__ == '__main__':
    import time

    time1 = time.time()

    q = query(fin_idx_ana.ASS_DEBT).filter(fin_idx_ana.gpcode == '000001.XSHE')
    df = get_fundamentals(q, '2016-10-01', '3q')
    print df
    
    q = query(com_share_stru.TOT_SHARE).filter(com_share_stru.gpcode == '000001.XSHE')
    df = get_fundamentals(q, '2016-10-01', '3q')
    print df

    time2 = time.time()
    print time2 - time1
